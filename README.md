![ES Project Logo](logo/export/master.png)

## About the initiative / Über die Initiative

The initiative was initially started for content and scripts for Adobe [ExtendScript][1] and [CEP][2]. Today, we provide free and paid services for Adobe ExtendScript, Adobe CEP, XML for Adobe [InDesign][3], and its workflows connected to tools like [WoodWing][9], [vjoon K4][10], or [SAP][11]. 

Diese Initiative wurde zunächst für Inhalte und Skripte für Adobe [ExtendScript][1] und [CEP][2] gestartet. Heute bieten wir kostenlose und kostenpflichtige Dienste für Adobe ExtendScript, Adobe CEP, XML für Adobe [InDesign][3] und die verbundenen Workflows an, die mit Produkten wie [WoodWing][9], [vjoon K4][10] oder [SAP][11] zusammenhängen.

## Quickstart guide

* ***To learn Adobe ExtendScript and XML***, please navigate to the public [documentation](https://extendscript-cep.gitlab.io/documentation/).
* To get familiar with the ***ESCEP XML workflow for popular CMS***, please head over to the public [workflow documentation]().
* To use the ***WordPress Plugin***, please click [here][6].

## Projects & Services

***All our projects are free and open source***, primarily licensed under MIT, GPLv3, and Creative Commons. ***If you need support*** for the application of our projects or technical consulting for your XML or publishing workflow, ***you can book our services***. Please get in touch with us at `info@pantara.ch`.

|   Type  |          Title           |                       Description                        | Lang. | Free | Paid |
|---------|--------------------------|----------------------------------------------------------|-------|------|------|
| Project | [Documentation][4]       | Einführung in JavaScript, ExtendScript, XML              | DE    | x    |      |
| Project | [ESCEP CMS Workflow][14] | Basic workflow for all other projects and services       | EN    | x    |      |
| Project | [ESCEP CMS Core][5]      | XML-Workflow from any source to CMS (WordPress, Drupal)  | EN    | x    |      |
| Project | [ESCEP CMS WP][6]        | WordPress Plugin to import XML content and assets        | EN    | x    |      |
| Project | ESCEP CMS Drupal         | Module to import XML content and assets (not yet public) | EN    | x    |      |
| Service | Software/XML Dev.        | Software and XML/XSL development                          | EN/DE |      | x    |
| Service | Project consulting       | Official Support for our projects incl. SLA              | EN/DE |      | x    |
| Service | Workflow consulting      | Techn. Consulting/realization of publishing workflows    | EN/DE |      | x    |
| Service | Courses                  | Courses Adobe ExtendScript, XML, publishing workflows    | EN/DE |      | x    |

## Roadmap

| Status | Timeline |       Project       |                               Task                              |
|--------|----------|---------------------|-----------------------------------------------------------------|
| ✅      | Q4 2022  | [escep-cms-core][5] | Start of public beta                                            |
| ✅      | Q4 2022  | [escep-cms-wp][6]   | Start of public beta                                            |
| ⏱      | Q3 2025  | escep-cms-drupal    | Start of public beta                                            |
| ⏱      | Q1 2025 | [Documentation][4]  | Content updates                                                 |
| 🚧      | Q1 2025  | [escep-cms-core][5] | End of beta, first stable release                               |
|        | Q1 2025  | [escep-cms-wp][6]   | End of beta, first stable release, listed on [wordpress.org][7] |
|        | Q1 2025  | escep-cms-drupal    | End of beta, first stable release, listed on [drupal.org][8]    |

<small>_`🚧`: In Progress/preparation, `✅`: Done/Released, `⏱`: Late or delayed, `❌`: Canceled_</small>

## Lead

* Head of development: [ansi labs GmbH][12]
* Head of consulting and sales: [Pantara GmbH][15]

## Official Partners

<table>
  <tr>
    <td width="32%">
      <figure>
        <a href="https://www.ansilabs.ch">
          <img src="https://www.ansilabs.ch/img/logo.png" alt="Logo ansi labs GmbH" width="120px"/>
        </a>
          <figcaption>Project lead</figcaption>
      </figure>
    </td>
    <td width="32%">
      <figure>
        <a href="https://www.ansilabs.ch">
          <img src="https://www.ansilabs.ch/img/logo.png" alt="Logo ansi labs GmbH" width="120px"/>
        </a>
        <figcaption>Head of consulting and sales</figcaption>
      </figure>
    </td>
    <td width="32%">
      <figure>
        <a href="https://publisher.ch">
          <img src="https://previous.publisher.ch/medien/logo_publisher_mob.png" alt="Logo Publisher" />
        </a>
        <figcaption>Official Media Partner</figcaption>
      </figure>
    </td>
  </tr>
</table>

[1]:https://en.wikipedia.org/wiki/ExtendScript
[2]:https://github.com/Adobe-CEP
[3]:https://www.adobe.com/products/indesign.html
[4]:https://gitlab.com/extendscript-cep/documentation
[5]:https://gitlab.com/extendscript-cep/escep-cms-core
[6]:https://gitlab.com/extendscript-cep/escep-cms-wp
[7]:https://wordpress.org/plugins/
[8]:https://www.drupal.org
[9]:https://www.woodwing.com
[10]:https://vjoon.com
[11]:https://www.sap.com/
[12]:https://www.ansilabs.ch
[13]:https://publisher.ch/
[14]:https://gitlab.com/extendscript-cep/escep-cms-workflow
